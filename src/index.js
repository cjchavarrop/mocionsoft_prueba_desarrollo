import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

// Importar librerias adicionadas en yarn
import 'bootstrap/dist/css/bootstrap.css'

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

