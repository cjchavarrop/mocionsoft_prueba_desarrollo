// Importar librerias del Core
import React, {useState, useEffect} from 'react'

// Importar librerias adicionadas en yarn
import {Link} from 'react-router-dom'
import axios from 'axios'

const Questions = () => {

    // USESTATE

    // Mantener información de la consulta de la API
    const [info, setInfo] = useState([])
    // Mantener información de la pregunta que se esta renderizando
    const [question, setQuestion] = useState([])
    // Se actualiza de uno en uno en la medida que se responda (true o false)
    const [conteo, setConteo] = useState(0)
    // Respuesta obtenidas por el usuario
    const [answer, setAnswer] = useState([])
    // Conteo de respuestas correctas
    const [correct, setCorrect] = useState(0)
    // Preguntas pendientes
    const [end, setEnd] = useState(false)


    // FUNCIONES

    // Capturar respuesta de la API mediante axios.
    const getData = async () => {
        const res = await axios.get('https://opentdb.com/api.php?amount=10&difficulty=hard&type=boolean')
        const data = await res.data
        const result = await data.results

        // Poblar array total información
        for (let i = 0; i < result.length; i++){
            var questionIni = result[i].question
            var question_clean = questionIni.replaceAll('&quot;','"').replaceAll("&#039;",'´').replaceAll('&ldquo;','"').replaceAll('&rdquo;','"')
            if(i === 0){
                setInfo(info => [...info,
                    {
                    category: result[i].category,
                    question: question_clean,
                    correct_answer: result[i].correct_answer
                    }
                ])
                // Poblar la preguntar que actual, esta enlazado con el conteo, unicamente se pobla la pregunta inicial desde esta función, el resto
                    // se van cargando por la función para adelantar encuesta. Por eso se usa con la posición 0 del objecto
                setQuestion(
                    {
                    category: result[i].category,
                    question: question_clean,
                    correct_answer: result[i].correct_answer
                    }
                )
                
            }else{
                setInfo(info => [...info,
                    {
                    category: result[i].category,
                    question: question_clean,
                    correct_answer: result[i].correct_answer
                    }
                ])
            }
        }
    }

    // Función cuando la respuesta es afirmativa
    const trueResponse = () => {
        nextQuestion('True')
    
    // Función cuando la respuesta es negativa
    }
    const falseResponse = () => {
        nextQuestion('False')
    }

    // Función para evaluar si estan pendientes mas preguntas, dispara la función para validar resultado y actualiza la pregunta en el useState question
    const nextQuestion = (response) => {
        
        if( conteo+1 < info.length ){
            setConteo(conteo+1)
            questionRefresh(conteo+1)
            validate(response)
        }else{
            setEnd(true)
            validate(response)
        }
         
    }

    // Función para evaluar si la respuesta es correcta y almacena las respuestas dadas por el usuario
    const validate = (response) => {

        var check = 'false'

        if(response === question.correct_answer){
            setCorrect(correct + 1)
            check = 'true'
        }

        setAnswer(answer => [...answer,
            {
                question: question.question,
                response: check,
                res_correct: question.correct_answer
            }
        ])        
    }

    // Función para actualizar el useState question con la pregunta que se debe renderizar, esta funcion reemplaza y debe estar en linea con lo que se esta renderizando
    const questionRefresh = (i) => {        
        if(i){
            setQuestion({
                category: info[i].category,
                question: info[i].question,
                correct_answer: info[i].correct_answer
            })
        }

        
    }

    // Funciones que se ejecutan tan pronto cargue el DOM
    useEffect( () => {
        getData()
    },[])



    return (
        <div>
            
            <div className="container">

                <div className="card text-center card border-primary mb-3">
                    
                    {/* HEADER */}
                    <div className="card-header">
                        {end ?

                        // codigo ejecutado cuando no hay mas encuestas por realizar
                        <div>
                            <h2>You scored</h2>
                            <h3>{correct} / {info.length}</h3>
                        </div>
                        
                        // codigo ejecutado si hay preguntas pendientes por resolver
                        :
                        <h1>{question.category}</h1>
                        }
                    </div>

                    {end ?
                    
                    // Aplica cuando aun hay preguntas pendientes por responder por parte del usuario
                        <div>
                            {/* BODY  */}
                            <div className="row">
                                <div className="col">
                                    <ul className="list-group">
                                        {answer.map( (item, index) => 
                                        
                                        <li key={index} className={item.response === 'true' ?
                                            'list-group-item list-group-item-success' 
                                            : 'list-group-item list-group-item-danger'}
                                            >{item.question} <span className="font-weight-bold">({item.res_correct})</span>
                                        </li>
                                        )}
                                    </ul>
                                    
                                </div>
                            </div>

                            {/* FOOTER  */}
                            <div className="card-footer text-muted">
                                <Link to={`/`} className="btn btn-primary">PLAY AGAIN</Link>
                                
                            </div>
                        </div>
                    
                    :
                    // Aplica cuando el usuario ya indico todas las respuestas, se renderiza los resultados obtenidos 
                        <div>
                            {/* BODY  */}
                            <div className="tarjeta-altura">
                                <div className="tarjeta-altura-interno">
                                    <div className="card-body">
                                        <h4 className="card-title">{question.question}</h4> 
                                        <div className="d-grid gap-2 mt-5">
                                            <button type="button" className="btn btn-primary" onClick={trueResponse}>True</button>
                                            <button type="button" className="btn btn-info" onClick={falseResponse}>False</button>

                                        </div>
                                        {/* <Link to={`/questions`} className="btn btn-primary">BEGIN</Link> */}
                                    </div>
                                </div>
                            </div>

                            {/* FOOTER  */}
                            <div className="card-footer text-muted">
                                <h3>
                                {conteo+1+" / "+info.length}
                                </h3>
                                
                            </div>
                        </div>
                    }
                    
                </div>
            
            </div>
        </div>
        
    )
}

export default Questions
