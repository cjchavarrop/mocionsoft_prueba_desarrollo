// Importar librerias del Core
import React from 'react'

// Importar librerias adicionadas en yarn
import {Link} from 'react-router-dom'

// Importar estilos personalizados
import '../estilos/estilos.css'


// Homepage, corresponde a la Route "/". Pagina inicial que redirecciona al usuario si quiere iniciar a la Route "/questions" donde se encuentra toda la logica
const Homepage = () => {


    return (
        <div className="container">

            <div className="card text-center card border-primary mb-3">
                <div className="card-header">
                    <h1>
                    Welcome to the Trivial Challenge
                    </h1>
                </div>
                <div className="tarjeta-altura">
                    <div className="card-body">
                        <h4 className="card-title">You will be presented with 10 True or False questions.</h4>
                        <h5 className="card-text">Can you score 100%?</h5>
                        <Link to={`/questions`} className="btn btn-primary">BEGIN</Link>
                    </div>
                </div>
                <div className="card-footer text-muted">
                    <h3>
                    Good luck!
                    </h3>
                    
                </div>
            </div>
            
        </div>
    )
}

export default Homepage
