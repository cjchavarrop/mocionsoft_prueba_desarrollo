// Importar librerias del Core
import React from 'react'

// Importar librerias adicionadas en yarn
import {
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom'

// Importar componentes propios
import Homepage from './components/Homepage'
import Questions from './components/Questions'

// Se manejaran 2 Route, la primera "/"" como pagina inicial donde se le pregunta al usuario si quiere hacer las encuestas y 
  // la segunda "/questions" donde se renderizaran las preguntas y se mostrara el resultado final
function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <Homepage/>
        </Route>
        <Route exact path="/questions">
          <Questions/>
        </Route>

      </Switch>

    </Router>
  );
}

export default App;
